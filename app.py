from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from pymongo import ReturnDocument
from datetime import datetime
from bson.objectid import ObjectId
from libs.users import User

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'sales'
#app.config['MONGO_URI'] = 'mongodb://localhost:27017/sales'
app.config['MONGO_URI'] = 'mongodb://datastore:27017/sales'

mongo = PyMongo(app)



@app.route('/user', methods=['POST'])
def user_add():
    email = request.json['email']
    nombre = request.json['nombre']
    apellido = request.json['apellido']
    direccion = request.json['direccion']

    u = User(email, nombre, apellido, direccion)
    if u.validate():

        user = mongo.db.users
        user_id = user.insert({'email': email, 'nombre': nombre, 'apellido': apellido, 'direccion': direccion, 'active': True})
        new_user = user.find_one({'_id': user_id})
        output = {'email': new_user['email'], 'nombre': new_user['nombre'], 'apellido': new_user['apellido'], 'direccion': new_user['direccion'], 'active': new_user['active']}
        return jsonify({'result': output}), 201
    else:
        output = {'error_message': u.error_message}
        return jsonify({'result': output}), 400



@app.route('/user/<email>', methods=['GET'])
def get_one_user(email):
    users = mongo.db.users
    u = users.find_one({'email': email})
    if u:
      output = {'email': email, 'nombre': u['nombre'], 'apellido': u['apellido'], 'direccion': u['direccion'], 'active': u['active']}
      status_code = 200
    else:
      output = 'No such user'
      status_code = 404
    return jsonify({'result': output}), status_code


@app.route('/user/<email>', methods=['PUT'])
def user_edit(email):
    nombre = request.json['nombre']
    apellido = request.json['apellido']
    direccion = request.json['direccion']

    update = {}
    if nombre:
        update['nombre'] = nombre
    if apellido:
        update['apellido'] = apellido
    if direccion:
        update['direccion'] = direccion

    user = mongo.db.users

    u = user.find_one_and_update(
        {'email': email},
        {'$set': update},
        None,
        None,
        return_document=ReturnDocument.AFTER
    )
    if u:
        return jsonify({'result': 'updated'}), 200
    else:
        return jsonify({'result': 'user don\'t exists!'}), 400




@app.route('/user/<email>', methods=['DELETE'])
def user_deactivate(email):
    users = mongo.db.users
    u = users.find_one_and_update({'email':email}, {'$set':{'active':False}}, None, None, return_document=ReturnDocument.AFTER)
    if u:
        return jsonify({'result': "Deleted!"}), 200
    else:
        return jsonify({'result': "user not found"}), 404


@app.route('/user/<email>', methods=['PATCH'])
def user_activate(email):
    users = mongo.db.users
    u = users.find_one_and_update({'email':email}, {'$set':{'active':True}}, None, None, return_document=ReturnDocument.AFTER)
    if u:
        return jsonify({'result': "Activated!"}), 200
    else:
        return jsonify({'result': "user not found"}), 404


@app.route('/sale', methods=['POST'])
def sale_add():
    user_email = request.json['user_email']
    users = mongo.db.users
    user = users.find_one({'email': user_email, 'active': True})
    if user:
        amount = request.json['amount']
        date = datetime.now().strftime("%Y-%m-%d %H:%M")
        sales = mongo.db.sales
        sale_id = sales.insert({'user_email': user_email , 'amount': amount, 'date': date, 'active': True})
        new_sale = sales.find_one({'_id': sale_id})
        output = {'uuid': str(sale_id),
                  'user_email': new_sale['user_email'],
                  'amount': new_sale['amount'], 
                  'date': new_sale['date'],
                  }
        status_code = 201
    else:
        output = "User doesn't exists"
        status_code = 404

    return jsonify({'result': output}), status_code


@app.route('/sale/<id>', methods=['GET'])
def sale_get_one(id):
    sales = mongo.db.sales
    if len(id)==24:
        s = sales.find_one({'_id': ObjectId(id)})

    else:
        s = None
    if s:
        output = {'_id': id, 'user_email': s['user_email'], 'amount': s['amount'], 'date': s['date'],
                  'active': s['active']}
        status_code = 200
    else:
        output = {'error_message': 'No such sale'}
        status_code = 404
    return jsonify({'result': output}), status_code

@app.route('/sales/<user_email>', methods=['GET'])
def sales_of_user(user_email):
    sales = mongo.db.sales
    s = sales.find({'user_email': user_email})
    output = []
    if s:
        for sale in s:
            output.append({'_id': str(sale['_id']),
                           'user_email': sale['user_email'],
                           'amount': sale['amount'],
                           'date': sale['date'],
                           'active': sale['active']})
            status_code = 200
    else:
        output = 'No sales for this user email'
        status_code = 404
    return jsonify({'result': output}), status_code



@app.route('/sale/<id>', methods=['DELETE'])
def sale_deactivate(id):
    sales = mongo.db.sales
    s = sales.find_one_and_update({'_id': ObjectId(id) }, {'$set':{'active':False}}, None, None, return_document=ReturnDocument.AFTER)
    if s:
        return jsonify({'result': "Deleted!"}), 200
    else:
        return jsonify({'result': "sale not found!"}), 404


@app.route('/user', methods=['GET'])
def users():
    users = mongo.db.users
    sales = mongo.db.sales
    output = []
    for u in users.find():
        total_sales = 0
        total_account = 0
        s = sales.find({'user_email': u['email']})
        for sale in s:
            total_sales+=1
            if sale['active'] == True:
                total_account += float(sale['amount'])

        output.append({'email': u['email'], 'nombre': u['nombre'], 'apellido': u['apellido'],
                       'direccion': u['direccion'], 'active': u['active'], 'ventas_asociadas': total_sales,
                       'importe_total': total_account})

    return jsonify({'result': output}), 200


@app.route('/sales', methods=['GET'])
def sales():
    sales = mongo.db.sales
    output = []
    for s in sales.find():
        output.append({'user_email': s['user_email'], 'amount': s['amount'], 'uuid': str(s['_id']), 'active': s['active'] })

    return jsonify({'result': output}), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)