# version 1.0

FROM  ubuntu:16.04
MAINTAINER Gerardo Benitez "gerardobenitez@gmail.com"

# mongodb
RUN apt-get update && apt-get -y install apt-transport-https curl
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
RUN echo 'deb https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse' | tee /etc/apt/sources.list.d/mongodb-org-3.6.list
RUN apt-get update
RUN apt-get install -y mongodb-org

# Create the default data directory
RUN mkdir -p /data/db

# Expose the default port
EXPOSE 27017
CMD ["--port 27017", "--smallfiles"]
# Set default container command
ENTRYPOINT ["/usr/bin/mongod"]


RUN apt-get install -y build-essential python3 python3-dev python3-pip python3-venv
RUN apt-get install -y git


# update pip
RUN python3 -m pip install pip --upgrade
RUN python3 -m pip install wheel

RUN git clone https://gitlab.com/g3r4benitez/salesrestfull.git
ADD . /salesrestfull
WORKDIR salesrestfull
RUN pip install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["app.py"]
