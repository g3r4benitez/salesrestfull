import re


class User():

    error_message = ""

    def __init__(self, email, name, lastname, address):
        self.email = email
        self.name = name
        self.lastname = lastname
        self.address = address

    def validate(self):
        """Validations"""

        # empty email
        if len(self.email) == 0:
            self.error_message += "Email is empty "

        # invalid email
        # it has exactly one @ sign, and at least one . in the part after the @
        if not re.match(r"[^@]+@[^@]+\.[^@]+", self.email):
            self.error_message += "Email is not valid "

        if self.error_message != "":
            return False
        else:
            return True

